﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSharpTestTask.Models
{
    public class ProductList
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Price { get; set; }
        public string Sorting { get; set; }
    }
    public class ProductDetail 
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Specs { get; set; }
        public string Availability { get; set; }
    }
    public class ViewTaskModel
    {
        public IEnumerable<ProductList> ProductLists { get; set; }
        public IEnumerable<ProductDetail> ProductDetails { get; set; }
    }
}