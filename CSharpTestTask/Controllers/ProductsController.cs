﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CSharpTestTask.Models;

namespace CSharpTest.Controllers
{
    public class ProductsController : Controller
    {
        // GET: ProductList
        public ActionResult Index()
        {
            if (TempData["productData"] == null)
            {
                ViewBag.Message = "C# Test task";
                ViewBag.ShowList = false;
                return View();
            }
            else
            {
                ViewBag.Message = "C# Test task";
                List<ProductsModel> prdlst = (List<ProductsModel>)TempData["productData"];
                ViewBag.ShowList = true;
                return View(prdlst);
            }
        }
        [HttpPost]
        public ActionResult Upload()
        {
            ViewBag.Message = "C# Test task";
            try
            {
                List<ProductsModel> productList = new List<ProductsModel>();
                var file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.Load(file.InputStream);
                    ProductsModel product;
                    XmlNodeList productnodes = xmldoc.SelectNodes("Store/Products/Product");
                    foreach (XmlNode prdn in productnodes)
                    {
                        product = new ProductsModel();

                        product.Title = prdn["Title"].InnerText;
                        product.Description = prdn["Description"].InnerText;
                        product.Image = prdn["Image"].InnerText;
                        //product.Price = prdn["Price"].InnerText;

                        if (prdn.SelectSingleNode(".//Price") != null)
                        {
                            product.Price = prdn["Price"].InnerText;
                        }

                        XmlNodeList sortingNodes = prdn.SelectNodes("Sorting");
                        foreach (XmlNode sortn in sortingNodes)
                        {
                            if (sortn["Popular"].InnerText != null)
                            {
                                product.Popular = sortn["Popular"].InnerText;
                            }
                        }
                        XmlNodeList specsNodes = prdn.SelectNodes("Specs");
                        foreach (XmlNode specn in specsNodes)
                        {
                            if (specn["Spec"].InnerText != null)
                            {
                                product.Spec = specn["Spec"].InnerText;
                            }
                        }
                        productList.Add(product);
                    }
                    TempData["productData"] = productList;
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View("Error", new HandleErrorInfo(ex, "ProductsController", "Upload"));
            }
        }
    }
}