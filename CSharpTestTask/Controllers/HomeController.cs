﻿using CSharpTestTask.Models;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Collections.Generic;
using System.Dynamic;

namespace CSharpTestTask.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "C# Test task";
            ViewTaskModel taskmodel = new ViewTaskModel
            {
                ProductLists = GetProductLists(),
                ProductDetails = GetProductDetails()
            };
            return View(taskmodel);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult IndexTaskModel()
        {
            ViewBag.Message = "C# Test task";
            ViewTaskModel taskmodel = new ViewTaskModel
            {
                ProductLists = GetProductLists(),
                ProductDetails = GetProductDetails()
            };
            return View(taskmodel);
        }

        public List<ProductList> GetProductLists()
        {
            List<ProductList> productlists = new List<ProductList>();
            XmlDocument doc = new XmlDocument();
            doc.Load(Server.MapPath("~/XML/List.xml"));
            foreach (XmlNode node in doc.SelectNodes("Store/Products/Product"))
            {
                productlists.Add(new ProductList
                {
                    Title = node["Title"].InnerText,
                    Description = node["Description"].InnerText,
                    Image = node["Image"].InnerText,
                    Price = node["Price"].InnerText,
                    Sorting = node["Sorting"].InnerText
                });
            }
            return productlists;
        }
        public List<ProductDetail> GetProductDetails()
        {
            List<ProductDetail> productdetails = new List<ProductDetail>();
            XmlDocument doc = new XmlDocument();
            doc.Load(Server.MapPath("~/XML/Detail.xml"));
            foreach (XmlNode node in doc.SelectNodes("Store/Products/Product"))
            {
                productdetails.Add(new ProductDetail
                {
                    Title = node["Title"].InnerText,
                    Description = node["Description"].InnerText,
                    Image = node["Image"].InnerText,
                    Specs = node["Specs"].InnerText,
                    Availability = node["Availability"].InnerText
                });
            }
            return productdetails;
        }
    }
}